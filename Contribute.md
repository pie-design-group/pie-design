# PieDesign 组件贡献规范

> 为保证PieDesign仓库代码规范性以及组件质量，请所有参与贡献的开发者详细阅读本规范，
> 并检查编写组件中是否存在背离规范的代码

## 代码规范

### 许可证声明

所有文件必须以许可证声明开头，其中必须包含的内容为：

```typescript
/**
 * Copyright 2023 Pie Design Group
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
```

此外，所有新创建的文件必须在上述注释末尾添加负责人、邮箱、创建时间，完整示例如下：

```typescript
/**
 * Copyright 2023 Pie Design Group
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created By Guo Tingjin<dev@peercat.cn> on 2023/12/17 13:09
 */
```

### 变量

#### 变量命名

- 所有变量名必须符合驼峰命名法
    - 对于静态变量，使用大驼峰命名法 例：`private static ExampleVariable: number = 0`
    - 对于非静态变量，使用小驼峰命名法 例：`let exampleVariable: number = 0;`
- 私有变量应当在变量名前加下划线 `_`

#### 变量声明

- 若变量在上下文中不变，则使用const声明常量
- 变量必须在被声明时就初始化，并标注类型，即使编译器可以通过上下文推断该变量的类型
- 对于函数型变量，优先通过在初始化时提供一个空函数进行初始化
- 对于使用 `@BuilderParam` 标记的函数，必须指明所有可提供的参数，而非使用 `Function` 标注其类型

### 组件

#### 组件命名

- 为避免与原生组件冲突以及提高代码可读性，组件名需以`Pie`开头，如`PiePrimaryButton`/`PieCalenderView`
- 组件名须符合大驼峰命名法

#### 组件参数

- 组件应当给尽可能多的组件参数赋默认值，默认值应当能够满足大部分时候的使用场景中的需要
- 组件颜色需使用AppStorage中存储的全局色彩变量，以方便使用者利用PieColorManager管理全局颜色
- 组件字号、字形、圆角等参数请使用PieDesign内置的 `PieFontStyle` 及 `PieRadius` 等预设参数

## 文档规范

完整的文档应当包含以下内容

- 组件使用场景举例
- 组件支持的参数说明
    - 参数名
    - 参数类型
    - 参数用途
    - 是否可空
    - 默认行为 (当可空时)
- 组件使用实例及实机截图 (大于等于两种)

文档图片资源应存放于 docs/images 中

## 合并审查

### 完整性

> 新组件在合并前，需依据本规范通过完整性审查

完整的组件应当包含但不限于以下内容，假设某新组件名称为[NewComponent]，其组件类型为[componentType]

- 说明文档：`docs/[componentType]/[NewComponent].md`
- 组件本身：`libPieDesign/src/main/ets/components/[componentType]/[NewComponent].ets`
- 组件导出：`libPieDesign/Index.ets`

### 规范性

> 新组件在合并前，需根据本规范通过规范性审查

未通过规范性审查的组件，将在PR中标注出不合规之处，待PR发起人修改完毕后进行二次审查