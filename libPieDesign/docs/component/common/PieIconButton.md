# PieIconButton

PieDesign图标按钮

## 导入模块

```typescript
import { PieIconButton } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieIconButton

PieIconButton(options?: PieIconButtonOptions)

**装饰器类型：**@Component

**参数**

| 名称                    | 类型                                        | 必填  | 装饰器类型 | 说明                                           |
|-----------------------|-------------------------------------------|-----|-------|----------------------------------------------|
| radius                | number                                    | 否   | @Prop | 图标按钮圆角大小<br/>默认值：4                           |
| alignMode             | PieIconButtonAlign                        | 否   | @Prop | 图标按钮布局模式<br/>默认值：PieIconButtonAlign.Vertical |
| iconSize              | number                                    | 否   | @Prop | 图标按钮图标大小<br/>默认值：24                          |
| descriptionSize       | number                                    | 否   | @Prop | 图标按钮文本大小<br/>默认值：`PieFontSize.Small`         |
| stateEffect           | boolean                                   | 否   | @Prop | 图标按钮状态效果<br/>默认值：true                        |
| icon                  | ResourceStr                               | 是   | 无     | 图标按钮图标                                       |
| iconColor             | ResourceColor                             | 否   | 无     | 图标按钮中，图标的颜色。当且仅当图标为svg格式时有效                  |
| description           | ResourceStr                               | 否   | 无     | 图标按钮文本说明<br/>默认值：`""`                        |
| customBackgroundColor | ResourceColor                             | 否   | 无     | 图标按钮背景颜色<br/>默认值：`PieColor.Background`       |
| customTextColor       | ResourceColor                             | 否   | 无     | 图标按钮文本颜色<br/>默认值：`PieColor.Text`             |
| mPadding              | Length, Padding                           | 否   | 无     | 图标按钮padding<br/>默认值：4                        |
| callback              | (e: ClickEvent) => void                   | 否   | 无     | 图标按钮单击回调                                     |
| hovered               | (isHover: boolean, e: HoverEvent) => void | 否   | 无     | 图标按钮悬浮回调                                     |

## PieIconButtonAlign枚举说明

| 名称         | 描述   |
|------------|------|
| Horizontal | 水平布局 |
| Vertical   | 垂直布局 |

## 事件

无

## 示例

```
import { PieColor, PieIconButton } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  @State count: number = 0;

  build() {
    Column() {
      Text("按钮已按" + this.count + "次")
      PieIconButton({
        icon: $r('app.media.daHuiYuan'),
        description: '大会员',
        iconColor: new PieColor(248, 146, 185).toHex(),
        callback: (e) => {
          this.count++;
          promptAction.showToast({ message: 'Hello PieIconButton' })
        }
      })
    }
    .width('100%')
  }
}
```

![preview](../../images/PieIconButton-1.png)

## [返回目录](../../README.md)