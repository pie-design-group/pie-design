# PieChatMessageBubble

PieDesign聊天消息气泡

## 导入模块

```typescript
import { PieChatMessageBubble } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieChatMessageBubble

PieChatMessageBubble(options?: PieChatMessageBubbleOptions)

**装饰器类型：**@Component

**参数**

| 名称                         | 类型                                  | 必填  | 装饰器类型       | 说明                                                                              |
|----------------------------|-------------------------------------|-----|-------------|---------------------------------------------------------------------------------|
| horizontalAlign            | PieChatMessageBubbleHorizontalAlign | 否   | @Prop       | 定义PieChatMessageBubble组件水平对齐<br/>默认值：`PieChatMessageBubbleHorizontalAlign.LEFT` |
| verticalAlign              | PieChatMessageBubbleVerticalAlign   | 否   | @Prop       | 定义PieChatMessageBubble组件垂直对齐<br/>默认值：`PieChatMessageBubbleVerticalAlign.TOP`    |
| messageObject              | Object                              | 否   | 无           | 定义组件消息结构<br/>默认值：`""`                                                           |
| radius                     | number                              | 否   | @Prop       | 定义组件默认消息体圆角<br/>默认值： `8`                                                        |
| mPadding                   | number                              | 否   | @Prop       | 定义组件默认消息体padding<br/>默认值： `12`                                                  |
| extendWidth                | boolean                             | 否   | @Prop       | 定义组件是否在水平上延展<br/>默认值： `true`                                                    |
| sender                     | string                              | 否   | @Prop       | 定义组件默认消息体发送人<br/>默认值： `""`                                                      |
| timeStamp                  | number                              | 否   | @Prop       | 定义组件默认消息体发送时间戳<br/>默认值： `-1`                                                    |
| hasShadow                  | boolean                             | 否   | @Prop       | 是否使用阴影<br/>默认值： `true`                                                          |
| shadowStyle                | ShadowStyle, ShadowOptions          | 否   | 无           | 定义组件阴影样式<br/>默认值： `{ radius: 5, color: this.mBorderColor }`                     |
| customBackgroundColor      | ResourceColor                       | 否   | 无           | 自定义组件消息背景颜色<br/>默认值：`PieColor.Box.toHex()`                                      |
| customTextColor            | ResourceColor                       | 否   | 无           | 自定义组件消息文本颜色<br/>默认值： `PieColor.Text.toHex()`                                    |
| customTextSecondaryColor   | ResourceColor                       | 否   | 无           | 自定义组件二级文本颜色<br/>默认值： `PieColor.TextSecondary.toHex()`                           |
| maxWidth                   | Length                              | 否   | @Prop       | 定义组件默认消息体最大宽度<br/>默认值： `"80%"`                                                  |
| avatar                     | ResourceStr                         | 否   | 无           | 定义组件显示的头像<br/>默认值： `null`                                                       |
| customMessageBodyBuilder   | (messageObj: Object) => void        | 否   | @BuildParam | 定义组件自定义消息体构造器<br/>默认值： this.defaultMessageBodyBuilder                           |
| customMessageSenderBuilder | (messageObj: Object) => void        | 否   | @BuildParam | 定义组件自定义发送者构造器<br/>默认值： this.defaultMessageSenderBuilder                         |
| customMessageDateBuilder   | (messageObj: Object) => void        | 否   | @BuildParam | 定义组件自定义尾部日期构造器<br/>默认值： this.defaultMessageDateBuilder                          |

## PieChatMessageBubbleHorizontalAlign枚举说明

| 名称    | 描述  |
|-------|-----|
| LEFT  | 左对齐 |
| RIGHT | 右对齐 |

## PieChatMessageBubbleVerticalAlign枚举说明

| 名称     | 描述   |
|--------|------|
| TOP    | 顶部对齐 |
| BOTTOM | 底部对齐 |

## 事件

无

## 示例

### 基础使用

```
import promptAction from '@ohos.promptAction';
import { PieChatMessageBubble, PieChatMessageBubbleHorizontalAlign } from '@ohos/libPie';

@Entry
@Preview
@Component
struct Index {
  messages: string[] = ["这是一条用PieChatMessageBubble展示的消息", "这是另一条用PieChatMessageBubble展示的消息", "这是右对齐的消息"]
  private shouldExit: boolean = false;

  onBackPress() {
    if (this.shouldExit) {
      return false;
    }
    this.shouldExit = true;
    setTimeout(() => {
      this.shouldExit = false;
    }, 2000);
    promptAction.showToast({ message: '再次返回退出' })
    return true;
  }

  build() {
    Column() {
      ForEach(this.messages, (message: string, index: number) => {
        Row() {
          PieChatMessageBubble({
            messageObject: message,
            sender: index == 2 ? "自己" : "对方",
            horizontalAlign: index == 2 ? PieChatMessageBubbleHorizontalAlign.RIGHT : PieChatMessageBubbleHorizontalAlign.LEFT,
            timeStamp: 160000000
          })
        }
      })
    }
    .width('100%')
  }
}
```

![preview](../../images/PieChatMessageBubble-1.png)

### 自定义消息结构的使用

```
import promptAction from '@ohos.promptAction';
import { PieChatMessageBubble, PieChatMessageBubbleHorizontalAlign } from '@ohos/libPie';

class CustomMessage {
  public sender: string = "自定义消息结构"
  public time: string = "2023/12/08 13:15"
  public message: string = "这是一条使用自定义消息结构和自定义构造器展示的消息，需要传入@Builder装饰的函数，和自定义messageObject一同使用"
}

@Entry
@Preview
@Component
struct Index {
  @State messages: string[] = ["这是一条用PieChatMessageBubble展示的消息", "这是另一条用PieChatMessageBubble展示的消息", "这是右对齐的消息"]
  customMessageObject: CustomMessage = new CustomMessage();
  private shouldExit: boolean = false;

  @Builder
  customMessageBuilder(customMessage: CustomMessage) {
    Column() {
      Text("发送人：" + customMessage.sender)
      Text("消息：" + customMessage.message)
    }
    .constraintSize({
      maxWidth: '80%'
    })
    .backgroundColor(Color.Gray)
  }

  onBackPress() {
    if (this.shouldExit) {
      return false;
    }
    this.shouldExit = true;
    setTimeout(() => {
      this.shouldExit = false;
    }, 2000);
    promptAction.showToast({ message: '再次返回退出' })
    return true;
  }

  build() {
    Column() {
      PieChatMessageBubble({
        messageObject: this.customMessageObject,
        customMessageBodyBuilder: this.customMessageBuilder,
        avatar: $r('app.media.app_icon')
      })
      PieChatMessageBubble({
        messageObject: "这是一条很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长很长的消息",
        sender: "对方",
        horizontalAlign: PieChatMessageBubbleHorizontalAlign.LEFT,
        timeStamp: new Date().getTime(),
        maxWidth: '70%',
        avatar: $r('app.media.app_icon')
      })
      ForEach(this.messages, (message: string, index: number) => {
        Row() {
          PieChatMessageBubble({
            messageObject: message,
            sender: index == 2 ? "自己" : "对方",
            horizontalAlign: index == 2 ? PieChatMessageBubbleHorizontalAlign.RIGHT : PieChatMessageBubbleHorizontalAlign.LEFT,
            timeStamp: 160000000,
            maxWidth: '80%',
            avatar: $r('app.media.app_icon')
          })
        }
      })
      Button('添加一条消息')
        .onClick(() => {
          this.messages.push("这是一条新消息")
        })
    }
    .width('100%')
  }
}
```

![preview](../../images/PieChatMessageBubble-2.png)

## [返回目录](../../README.md)