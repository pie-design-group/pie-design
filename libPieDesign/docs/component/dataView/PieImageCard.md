# PieImageCard

当需要展示图文结合的信息时，可以使用 `PieImageCard` 展示数据，例如：

- 视频入口
- 头条新闻入口
- 带有图片的笔记入口

## 导入模块

```typescript
import { PieImageCard } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieImageCard

PieImageCard(options?: PieImageCardOptions)

**装饰器类型：**@Component

**参数**

| 名称                 | 类型                                            | 必填  | 装饰器类型         | 说明                                                                                           |
|--------------------|-----------------------------------------------|-----|---------------|----------------------------------------------------------------------------------------------|
| pImage             | Resource                                      | 否   | 无             | 定义PieImageCard组件展示的图片<br/>**默认值：**`null`                                                     |
| pWidth             | Length                                        | 否   | 无             | 定义组件宽度<br/>**默认值：**`null` 跟随子组件自适应                                                           |
| pHeight            | Length                                        | 否   | 无             | 定义组件高度<br/>**默认值：**`null` 跟随子组件自适应                                                           |
| pLayoutMode        | PieImageCardLayoutMode                        | 否   | @Prop         | 定义组件布局方向<br/>**默认值：** PieImageCardLayoutMode.Vertical                                        |
| pImageHeight       | Length                                        | 否   | 无             | 定义组件内图片高度<br/>**默认值：** this.pLayoutMode == PieImageCardLayoutMode.Horizontal ? 100 : '100%'; |
| pImageWidth        | Length                                        | 否   | 无             | 定义组件内图片宽度<br/>**默认值：** this.pLayoutMode == PieImageCardLayoutMode.Horizontal ? '100%' : 100; |
| pTitle             | ResourceStr                                   | 否   | 无             | 定义组件默认标题<br/>**默认值：** ""                                                                     |
| pItem              | Object                                        | 否   | @Prop         | 定义customBuilder入参<br/>**默认值：** new Object()                                                  |
| pTouchEffect       | boolean                                       | 否   | @Prop         | 定义是否启用触摸动效<br/>**默认值：** true                                                                 |
| pDescription       | ResourceStr                                   | 否   | 无             | 定义组件默认描述文本<br/>**默认值：** ""                                                                   |
| pImageAspectRatio  | number                                        | 否   | @Prop         | 定义组件内图片宽高比<br/>**默认值：** -1                                                                   |
| pRadius            | Length                                        | 否   | 无             | 定义组件圆角大小<br/>**默认值：** PieRadius.Mini                                                         |
| pTitleSize         | number                                        | 否   | 无             | 定义组件默认标题文本大小<br/>**默认值：** PieFontSize.Subtitle3                                              |
| pHoverEffect       | HoverEffect                                   | 否   | 无             | 定义组件悬浮效果<br/>**默认值：** HoverEffect.Auto                                                       |
| pDescriptionSize   | number                                        | 否   | @Prop         | 定义组件默认描述文本文字大小<br/>**默认值：** PieFontSize.Subtitle3                                            |
| pTitleWeight       | FontWeight                                    | 否   | 无             | 定义组件默认标题字重<br/>**默认值：** PieFontWeight.Headline7                                              |
| titleMaxLines      | number                                        | 否   | @Prop         | 定义组件默认标题最大行数<br/>默认值：2                                                                       |
| pDescriptionWeight | FontWeight                                    | 否   | 无             | 定义组件默认描述文本字重<br/>**默认值：** PieFontWeight.Subtitle3                                            |
| pShadow            | ShadowOptions, ShadowStyle                    | 否   | @Prop         | 定义组件阴影样式<br/>**默认值：** ShadowStyle.OUTER_DEFAULT_SM                                           |
| pOnClick           | (e: ClickEvent) => void                       | 否   | @Prop         | 定义组件点击回调<br/>**默认值：** () => {}                                                               |
| pOnTouch           | (e: TouchEvent) => void                       | 否   | @Prop         | 定义组件触摸回调<br/>**默认值：** () => {}                                                               |
| pOnHover           | (isHover: boolean, event: HoverEvent) => void | 否   | @Prop         | 定义组件悬浮回调<br/>**默认值：** () => {}                                                               |
| customBuilder      | (param: T) => void                            | 否   | @BuilderParam | 定义组件自定义页面结构<br/>**默认值：** this.descriptionBody                                                |

## PieImageCardLayoutMode枚举说明

| 名称         | 描述   |
|------------|------|
| Vertical   | 纵向布局 |
| Horizontal | 横向布局 |

## 事件

无

## 示例

### 基础使用

```
import { PieImageCard, PieImageCardLayoutMode } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  @State message: string = 'Hello World';

  @Builder
  ImageCardBody(message: string) {
    Text(message)
  }

  build() {
    Row() {
      Column({ space: 12 }) {
        PieImageCard({
          pImage: "https://i1.hdslb.com/bfs/archive/b75392a07025fd4d6b17f079a37750399016ea85.jpg@672w_378h_1c_!web-home-common-cover.webp",
          pLayoutMode: PieImageCardLayoutMode.Vertical,
          pTitle: "送37个人头也能赢？大G和狗熊疯狂送人头，同样都是偷塔的，差距咋这么大呢？",
          pDescription: "我才是熊猫大G 12-6",
          pImageAspectRatio: 16 / 9,
          pOnClick: () => {
            promptAction.showToast({ message: 'Hello ImageCard' })
          },
          pWidth: '50%',
          pImageWidth: '100%'
        })
      }
      .width('100%')
    }
    .height('100%')
  }
}
```

![preview](../../images/PieImageCard-2.jpeg)

### 在ForEach中使用PieImageCard

```
import { PieImageCard } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  @State message: string = 'Hello World';
  private imageCardResourceArray: string[] = ['a', 'b', 'c']

  @Builder
  ImageCardBody(message: string) {
    Text(message)
  }

  build() {
    Row() {
      Column({ space: 12 }) {
        ForEach(this.imageCardResourceArray, (cardResource: string) => {
          PieImageCard({
            pImage: $r('app.media.startIcon'),
            pWidth: '100%',
            pLayoutMode: 'horizontal',
            pItem: cardResource,
            pOnClick: () => {
              promptAction.showToast({ message: 'Hello ImageCard' })
            },
            customBuilder: this.ImageCardBody,
            pThis: this
          })
        })
      }
      .width('100%')
    }
    .height('100%')
  }
}
```

![preview](../../images/PieImageCard-1.jpeg)

### 进阶用法

```
import { PieImageCard } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

class VideoObject {
  playCount: number = 4162;
  danmakuCount: number = 1;
  videoLength: string = "01:03:53";
  title = "OpenHarmony系统架构解析";
  up = "OpenHarmony开发者";
  publish: string = "5-16";
}

@Entry
@Preview
@Component
struct Index {
  @State message: string = 'Hello World';
  videoObject: VideoObject = new VideoObject();
  arr: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  @Builder
  overlayBuilder(video: VideoObject) {
    Row() {
      Row({ space: 8 }) {
        Row({ space: 2 }) {
          Image($r('app.media.playCount'))
            .size({ width: 16, height: 16 })
            .fillColor(Color.White)
          Text(video.playCount.toString())
            .fontSize(10)
            .fontColor(Color.White)
        }

        Row({ space: 2 }) {
          Image($r('app.media.damakuCount'))
            .size({ width: 16, height: 16 })
            .fillColor(Color.White)
          Text(video.danmakuCount.toString())
            .fontSize(10)
            .fontColor(Color.White)
        }
      }

      Text(video.videoLength)
        .fontSize(10)
        .fontColor(Color.White)
    }
    .linearGradient({
      colors: [
        [Color.Transparent, 0],
        ["#80000000", 1]
      ]
    })
    .width('100%')
    .justifyContent(FlexAlign.SpaceBetween)
    .padding(5)
  }

  @Builder
  descriptionBuilder(video: VideoObject) {
    Column({ space: 4 }) {
      Text(video.title)
        .maxLines(2)
        .textOverflow({ overflow: TextOverflow.Ellipsis })
        .fontSize(14)
        .width('100%')
      Row({ space: 12 }) {
        Row({ space: 4 }) {
          Image($r('app.media.up'))
            .fillColor(Color.Gray)
            .size({ width: 16, height: 16 })
          Text(video.up)
            .fontSize(12)
            .fontColor(Color.Gray)
        }

        Image($r('sys.media.ohos_ic_public_more'))
          .size({ width: 12, height: 12 })
      }
      .width('100%')
    }
    .height(72)
    .justifyContent(FlexAlign.SpaceBetween)
    .padding(8)
  }

  build() {
    Row() {
      Scroll() {
        Column() {
          Grid() {
            ForEach(this.arr, (_) => {
              GridItem() {
                PieImageCard({
                  pImage: "https://i0.hdslb.com/bfs/archive/c03982c67126f41703fc0b03fc65a9bfae5df6ba.jpg@672w_378h_1c_!web-search-common-cover.webp",
                  pImageAspectRatio: 16 / 9,
                  pOnClick: () => {
                    promptAction.showToast({ message: 'Hello ImageCard' })
                  },
                  pItem: this.videoObject,
                  imageOverlay: this.overlayBuilder,
                  customBuilder: this.descriptionBuilder
                })
              }
            })
          }
          .columnsTemplate('1fr 1fr')
          .columnsGap(8)
          .rowsGap(8)
          .edgeEffect(EdgeEffect.Spring)
          .width('100%')
          .height('100%')
          .padding(8)
        }
      }
    }
    .height('100%')
  }
}
```

![preview](../../images/PieImageCard-3.jpeg)

## [返回目录](../../README.md)