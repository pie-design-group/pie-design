# PieBreadcrumb

PieDesign面包屑，用于提示用户当前页面层级。

## 导入模块

```typescript
import { PieBreadcrumb } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieBreadcrumb

PieBreadcrumb(options: PieBreadcrumbOptions)

**装饰器类型：**@Component

**参数**

| 名称                | 类型                                               | 必填  | 装饰器类型         | 说明          |
|-------------------|--------------------------------------------------|-----|---------------|-------------|
| breadcrumbPath    | PieBreadcrumbPath[]                              | 否   | @Prop         | 面包屑路径       |
| breadcrumbSize    | number                                           | 否   | @Prop         | 路径文本大小      |
| iconSize          | number                                           | 否   | @Prop         | 路径图标大小      |
| breadcrumbPadding | number                                           | 否   | @Prop         | 路径padding大小 |
| customCrumb       | (path: PieBreadcrumbPath, index: number) => void | 否   | @BuilderParam | 自定义路径构造器    |
| customSplitter    | () => void                                       | 否   | @BuilderParam | 自定义路径分隔符    |

## PieBreadcrumbPath类型说明

| 名称        | 类型                                                           | 必填  | 描述                 |
|-----------|--------------------------------------------------------------|-----|--------------------|
| path      | ResourceStr                                                  | 是   | 路径名称               |
| endIcon   | ResourceStr                                                  | 否   | item中显示在右侧的图标信息路径。 |
| startIcon | ResourceStr                                                  | 否   | item中显示在左侧的图标信息路径。 |
| onClick   | (pathName: string, event: ClickEvent, index: number) => void | 否   | 路径点击回调             |

## 事件

不支持[通用事件](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-events-click.md/)

## 示例

```
import { PieBreadcrumb, PieHamburgerMenu } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  private shouldExit: boolean = false;

  onBackPress() {
    if (this.shouldExit) {
      return false;
    }
    this.shouldExit = true;
    setTimeout(() => {
      this.shouldExit = false;
    }, 2000);
    promptAction.showToast({ message: '再次返回退出' })
    return true;
  }

  build() {
    Column() {
      PieHamburgerMenu({
        menuStruct: [
          {
            content: '文件(F)',
            subMenu: [
              {
                content: '新建',
                subMenu: [
                  {
                    content: '新建项目'
                  },
                  {
                    content: '版本控制中的项目'
                  }
                ]
              },
              {
                content: '打开项目'
              },
              {
                content: '关闭项目'
              }
            ]
          },
          {
            content: '编辑(E)',
            subMenu: [
              {
                content: '撤销'
              },
              {
                content: '恢复'
              }
            ]
          },
          {
            content: '视图(V)',
            subMenu: [
              {
                content: '撤销'
              },
              {
                content: '恢复'
              }
            ]
          },
          {
            content: '导航(N)',
            subMenu: [
              {
                content: '撤销'
              },
              {
                content: '恢复'
              }
            ]
          },
          {
            content: '代码(C)',
            subMenu: [
              {
                content: '撤销'
              },
              {
                content: '恢复'
              }
            ]
          }
        ]
      })
      PieBreadcrumb({
        breadcrumbPath: [
          {
            path: 'pieDesign'
          },
          {
            path: 'entry'
          },
          {
            path: 'src'
          },
          {
            path: 'main'
          },
          {
            path: 'ets'
          },
          {
            path: 'pages'
          },
          {
            path: 'Index.ets',
            startIcon: $r('sys.media.ohos_ic_public_text')
          }
        ]
      })
        .margin({ left: 12 })
    }
    .alignItems(HorizontalAlign.Start)
    .width('100%')
  }
}
```

![Preview](../../images/PieBreadcrumb-1.png)

## [返回目录](../../README.md)