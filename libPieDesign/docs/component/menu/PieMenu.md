# PieMenu

PieDesign菜单

## 导入模块

```typescript
import { PieMenu } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieMenu

PieMenu(options: PieMenuOptions)

**装饰器类型：**@Component

**参数**

| 名称                   | 类型                                         | 必填  | 装饰器类型         | 说明                           |
|----------------------|--------------------------------------------|-----|---------------|------------------------------|
| menuStruct           | IPieMenuNode[]                             | 否   | 无             | 菜单结构<br/>默认值：[]              |
| fontSize             | number                                     | 否   | 无             | 菜单文本大小                       |
| menuSpace            | number                                     | 否   | 无             | 菜单间隔大小<br/>默认值：4             |
| radius               | Length                                     | 否   | 无             | 菜单圆角<br/>默认值：4               |
| customMenuBuilder    | (node: PieMenuNode, index: number) => void | 否   | @BuilderParam | 自定义菜单入口<br/>默认值： this.Menu   |
| customSubMenuBuilder | (nodes: PieMenuNode[]) => void             | 否   | @BuilderParam | 自定义子菜单<br/>默认值： this.SubMenu |

## IPieMenuNode类型说明

| 名称        | 类型                          | 必填  | 描述                     |
|-----------|-----------------------------|-----|------------------------|
| startIcon | ResourceStr                 | 否   | item中显示在左侧的图标信息路径。     |
| content   | ResourceStr                 | 是   | item的内容信息。             |
| endIcon   | ResourceStr                 | 否   | item中显示在右侧的图标信息路径。     |
| labelInfo | ResourceStr                 | 否   | 定义结束标签信息，如快捷方式Ctrl+C等。 |
| subMenu   | IPieMenuNode[]              | 否   | 子菜单结构                  |
| onClick   | (event: ClickEvent) => void | 否   | 菜单点击回调                 |

## PieMenuController

通过PieMenuController可以控制菜单的结构。一个PieMenuController对象只能控制一个PieMenu组件，PieMenuController绑定后，
才能调用PieMenuController上的方法。

### setMenu

setMenu: (nodes: PieMenuNode) => void;

设置菜单项

## 事件

不支持[通用事件](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-events-click.md/)

## 示例

PieDesign汉堡菜单的示例说明参考如下。

```
import { PieMenu } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  build() {
    Row() {
      PieMenu({
        menuStruct: [
          {
            content: '文件(F)',
            subMenu: [
              {
                content: '新建',
                subMenu: [
                  {
                    content: '新建项目'
                  },
                  {
                    content: '版本控制中的项目'
                  }
                ]
              },
              {
                content: '打开项目'
              },
              {
                content: '关闭项目'
              }
            ]
          }
        ]
      })
    }
    .height('100%')
  }
}
```

![preview](../../images/PieMenu-1.png)

## [返回目录](../../README.md)