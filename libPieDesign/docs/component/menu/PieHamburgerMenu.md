# PieHamburgerMenu

PieDesign汉堡菜单

## 导入模块

```typescript
import { PieHamburgerMenu } from "@ohos/pieDesign";
```

## 子组件

无

## 属性

支持[通用属性](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-size.md/)

## PieHamburgerMenu

PieHamburgerMenu(options: PieHamburgerMenuOptions)

**装饰器类型：**@Component

**参数**

| 名称                   | 类型                                         | 必填  | 装饰器类型         | 说明                                                 |
|----------------------|--------------------------------------------|-----|---------------|----------------------------------------------------|
| hamburgerIcon        | Resource                                   | 否   | 无             | 汉堡菜单图标<br/>默认值：$r('sys.media.ohos_ic_public_more') |
| menuStruct           | IPieMenuNode[]                             | 否   | 无             | 菜单结构<br/>默认值：[]                                    |
| controller           | PieMenuController                          | 否   | 无             | 汉堡菜单控制器                                            |
| fontSize             | number                                     | 否   | 无             | 菜单文本大小                                             |
| menuSpace            | number                                     | 否   | 无             | 菜单间隔大小<br/>默认值：4                                   |
| radius               | Length                                     | 否   | 无             | 菜单圆角<br/>默认值：4                                     |
| onStateChanged       | (extracted: boolean) => void               | 否   | 无             | 当菜单展开或收起时触发该回调                                     |
| customMenuBuilder    | (node: PieMenuNode, index: number) => void | 否   | @BuilderParam | 自定义菜单入口<br/>默认值： this.Menu                         |
| customSubMenuBuilder | (nodes: PieMenuNode[]) => void             | 否   | @BuilderParam | 自定义子菜单<br/>默认值： this.SubMenu                       |

## IPieMenuNode类型说明

| 名称        | 类型                          | 必填  | 描述                     |
|-----------|-----------------------------|-----|------------------------|
| startIcon | ResourceStr                 | 否   | item中显示在左侧的图标信息路径。     |
| content   | ResourceStr                 | 是   | item的内容信息。             |
| endIcon   | ResourceStr                 | 否   | item中显示在右侧的图标信息路径。     |
| labelInfo | ResourceStr                 | 否   | 定义结束标签信息，如快捷方式Ctrl+C等。 |
| subMenu   | IPieMenuNode[]              | 否   | 子菜单结构                  |
| onClick   | (event: ClickEvent) => void | 否   | 菜单点击回调                 |

## PieHamburgerMenuController

通过PieHamburgerMenuController可以控制汉堡菜单的显示/隐藏。一个PieHamburgerMenuController对象只能控制一个PieMenu组件，PieHamburgerMenuController绑定后，
才能调用PieHamburgerMenuController上的方法。

### expand

expand: () => void

展开汉堡菜单

### retract

retract: () => void

隐藏汉堡菜单

### toggle

toggle: () => void

若汉堡菜单已经显示，则隐藏汉堡菜单，否则展开汉堡菜单

### setHamburgerIcon

setHamburgerIcon: (icon: ResourceStr) => void

设置汉堡菜单按钮

### setMenu

setMenu: (nodes: PieMenuNode) => void;

设置菜单项

## 事件

不支持[通用事件](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/arkui-ts/ts-universal-events-click.md/)

## 示例

PieDesign汉堡菜单的示例说明参考如下。

```
import { PieHamburgerMenu } from '@ohos/libPie';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  @State message: string = 'Hello World';
  private shouldExit: boolean = false;

  onBackPress() {
    if (this.shouldExit) {
      return false;
    }
    this.shouldExit = true;
    setTimeout(() => {
      this.shouldExit = false;
    }, 2000);
    promptAction.showToast({ message: '再次返回退出' })
    return true;
  }


  build() {
    Row() {
      PieHamburgerMenu({
        menuStruct: [
          {
            content: '文件(F)',
            subMenu: [
              {
                content: '新建',
                subMenu: [
                  {
                    content: '新建项目'
                  },
                  {
                    content: '版本控制中的项目'
                  }
                ]
              },
              {
                content: '打开项目'
              },
              {
                content: '关闭项目'
              }
            ]
          }
        ]
      })
    }
    .height('100%')
  }
}
```

![preview](../../images/PieHamburgerMenu-1.png)

## [返回目录](../../README.md)