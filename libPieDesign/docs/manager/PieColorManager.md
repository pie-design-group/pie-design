# PieColorManager

颜色管理提供全局颜色基础能力，包括对颜色类别、颜色主题的管理。

## PieColorManager.getInstance

getInstance(): PieColorManager

获取颜色管理器。

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
```

## PieColorManager.store

store(): void

持久化当前色彩配置

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setPrimary(PieColor.White).store();
```

## PieColorManager.setPrimary

setPrimary(color: PieColor): PieColor

设置主题颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setPrimary(PieColor.White)
```

## PieColorManager.setSuccess

setSuccess(color: PieColor): PieColor

设置成功指示颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setSuccess(PieColor.White)
```

## PieColorManager.setWarning

setWarning(color: PieColor): PieColor

设置警告指示颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setWarning(PieColor.White)
```

## PieColorManager.setDanger

setDanger(color: PieColor): PieColor

设置危险指示颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setDanger(PieColor.White)
```

## PieColorManager.setText

setText(color: PieColor): PieColor

设置文本颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setText(PieColor.White)
```

## PieColorManager.setTextSecondary

setTextSecondary(color: PieColor): PieColor

设置二级文本颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setTextSecondary(PieColor.White)
```

## PieColorManager.setWeak

setWeak(color: PieColor): PieColor

设置弱颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setWeak(PieColor.White)
```

## PieColorManager.setLight

setLight(color: PieColor): PieColor

设置亮色颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setLight(PieColor.White)
```

## PieColorManager.setBorder

setBorder(color: PieColor): PieColor

设置边框颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setBorder(PieColor.White)
```

## PieColorManager.setBox

setBox(color: PieColor): PieColor

设置盒式组件背景颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setBox(PieColor.White)
```

## PieColorManager.setBackground

setBackground(color: PieColor): PieColor

设置背景颜色

**参数：**

| 参数名   | 类型       | 必填  | 说明     |
|-------|----------|-----|--------|
| color | PieColor | 是   | 目标颜色对象 |

**返回值：**

| 类型              | 说明     |
|-----------------|--------|
| PieColorManager | 颜色管理对象 |

**示例：**

```typescript
import { PieColorManager } from '@ohos/libPie';

let colorManager = PieColorManager.getInstance();
colorManager.getInstance().setBackground(PieColor.White)
```