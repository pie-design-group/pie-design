# PieColor

Pie Design 颜色类，提供颜色创建、管理的实用工具函数，作为全局颜色管理的参数类型。

## 常量

| 名称            | 类型       | 可读  | 可写  | 说明     |
|---------------|----------|-----|-----|--------|
| Primary       | PieColor | 是   | 否   | 主题颜色   |
| Success       | PieColor | 是   | 否   | 成功指示颜色 |
| Warning       | PieColor | 是   | 否   | 警告指示颜色 |
| Danger        | PieColor | 是   | 否   | 危险指示颜色 |
| White         | PieColor | 是   | 否   | 白色     |
| Text          | PieColor | 是   | 否   | 文本颜色   |
| TextSecondary | PieColor | 是   | 否   | 二级文本颜色 |
| Weak          | PieColor | 是   | 否   | 弱化文本颜色 |
| Light         | PieColor | 是   | 否   | 高亮文本颜色 |
| Border        | PieColor | 是   | 否   | 边框颜色   |
| Box           | PieColor | 是   | 否   | 容器背景颜色 |
| Background    | PieColor | 是   | 否   | 全局背景颜色 |
| SnowGray      | PieColor | 是   | 否   | 雪域灰    |

## constructor

constructor(r: number = 0, g: number = 0, b: number = 0, a: number = 1): PieColor

构建PieColor对象

**参数：**

| 参数名 | 类型     | 必填  | 说明   |
|-----|--------|-----|------|
| r   | number | 否   | 红色值  |
| g   | number | 否   | 绿色值  |
| b   | number | 否   | 蓝色值  |
| a   | number | 否   | 透明度值 |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | 颜色对象 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = new PieColor();
```

## PieColor.FromHex

FromHex(color: string): PieColor

从16进制颜色构建PieColor对象

**参数：**

| 参数名   | 类型     | 必填  | 说明                |
|-------|--------|-----|-------------------|
| color | string | 是   | 16进制颜色文本，`#`字符可缺省 |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | 颜色对象 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromHex("#FFF");
let color2 = PieColor.FromHex("000");
```

## PieColor.FromNumber

FromNumber(color: number): PieColor

从16进制颜色构建PieColor对象

**参数：**

| 参数名   | 类型     | 必填  | 说明      |
|-------|--------|-----|---------|
| color | number | 是   | 10进制颜色值 |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | 颜色对象 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromNumber(0);
```

## PieColor.FromResource

FromNumber(resourceMan: resourceManager.ResourceManager, resource: Resource): PieColor

从资源对象构建PieColor对象

**参数：**

| 参数名         | 类型                              | 必填  | 说明         |
|-------------|---------------------------------|-----|------------|
| resourceMan | resourceManager.ResourceManager | 是   | 应用上下文资源管理器 |
| resource    | Resource                        | 是   | 颜色资源       |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | 颜色对象 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
```

## PieColor.getRed

getRed(): number

**返回值：**

| 类型     | 说明  |
|--------|-----|
| number | 红色值 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.getRed();
```

## PieColor.getGreen

getGreen(): number

**返回值：**

| 类型     | 说明  |
|--------|-----|
| number | 绿色值 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.getGreen();
```

## PieColor.getBlue

getBlue(): number

**返回值：**

| 类型     | 说明  |
|--------|-----|
| number | 蓝色值 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.getBlue();
```

## PieColor.getAlpha

getAlpha(): number

**返回值：**

| 类型     | 说明   |
|--------|------|
| number | 透明度值 |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.getAlpha();
```

## PieColor.setRGB

setRGB(r: number, g: number, b: number): PieColor

**参数：**

| 参数名 | 类型     | 必填  | 说明  |
|-----|--------|-----|-----|
| r   | number | 是   | 红色值 |
| g   | number | 是   | 绿色值 |
| b   | number | 是   | 蓝色值 |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | this |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.setRGB(0, 255, 144).setRGB(255, 0, 0);
```

## PieColor.setAlpha

setAlpha(a: number): PieColor

**参数：**

| 参数名 | 类型     | 必填  | 说明   |
|-----|--------|-----|------|
| a   | number | 是   | 透明度值 |

**返回值：**

| 类型       | 说明   |
|----------|------|
| PieColor | this |

**示例：**

```typescript
import { PieColor } from '@ohos/libPie';

let color1 = PieColor.FromResource(getContext(this).resourceManager, $r('sys.color.white'));
let red = color1.setAlpha(0);
```
