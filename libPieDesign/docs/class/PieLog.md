# PieLog

PieDesign 工具函数，提供规范化日志输出能力。

## PieLog.getInstance

PieLog.getInstance(identifier: string = "default"): PieLog

获取或新建一个PieLog实例

**参数：**

| 参数名        | 类型     | 必填  | 说明          |
|------------|--------|-----|-------------|
| identifier | string | 否   | 默认值：default |

**返回值：**

| 类型     | 说明     |
|--------|--------|
| PieLog | 日志输出对象 |

## PieLog.info

info(tag: string, ...msg: Object[]): PieLog

输出信息类日志

**参数：**

| 参数名 | 类型     | 必填  | 说明              |
|-----|--------|-----|-----------------|
| tag | string | 是   | 日志标识，建议与页面或函数相关 |
| msg | Object | 否   | 日志内容，可以为任何类型    |

**返回值：**

| 类型     | 说明     |
|--------|--------|
| PieLog | 日志输出对象 |

## PieLog.debug

debug(tag: string, ...msg: Object[]): PieLog

输出调试类日志

**参数：**

| 参数名 | 类型     | 必填  | 说明              |
|-----|--------|-----|-----------------|
| tag | string | 是   | 日志标识，建议与页面或函数相关 |
| msg | Object | 否   | 日志内容，可以为任何类型    |

**返回值：**

| 类型     | 说明     |
|--------|--------|
| PieLog | 日志输出对象 |

## PieLog.error

error(tag: string, ...msg: Object[]): PieLog

输出错误类日志

**参数：**

| 参数名 | 类型     | 必填  | 说明              |
|-----|--------|-----|-----------------|
| tag | string | 是   | 日志标识，建议与页面或函数相关 |
| msg | Object | 否   | 日志内容，可以为任何类型    |

**返回值：**

| 类型     | 说明     |
|--------|--------|
| PieLog | 日志输出对象 |