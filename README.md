# PieDesign Pro

`Pie Design Pro` 是一个风格统一、轻量、注重用户体验和开发效率的OpenHarmony/HarmonyOS应用组件库

> `Pie Design`风格借鉴 `HarmonyOS`
> 组件设计请参考[HarmonyOS 设计指南](https://developer.harmonyos.com/cn/design/)

### 文档

[仓库内文档入口](https://gitee.com/pie-design-group/pie-design/blob/beta/libPieDesign/docs/README.md)

### 安装

#### 手动安装

- 前往[Release发布页面](https://gitee.com/pie-design-group/pie-design/releases)获取最新发行版
- 将下载到的har包放置于项目目录下，假设相对于项目根目录的路径为：`./lib/libPieDesign.har`
- 编辑项目`oh-package.json5`文件，在`dependencies`中添加依赖：

```json
{
  "dependencies": {
    "@pie/design": "file:./lib/libPieDesign.har"
  }
}
```

- 同步项目文件。

### 使用案例

>
其它组件及组件属性详细信息请参考[PieDesign使用文档](https://gitee.com/pie-design-group/pie-design/blob/beta/libPieDesign/docs/README.md)

#### 引入组件

```typescript
import { PieImageCard } from "@pie/design"
```

#### 在页面或组件build函数中使用组件

```
import { PieImageCard } from '@pie/design';
import promptAction from '@ohos.promptAction';

@Entry
@Preview
@Component
struct Index {
  private imageCardResourceArray: string[] = ['a', 'b', 'c']

  @Builder
  ImageCardBody(message: string) {
    Text(message)
  }

  build() {
    Row() {
      Column({ space: 12 }) {
        ForEach(this.imageCardResourceArray, (cardResource: string) => {
          PieImageCard({
            pImage: $r('app.media.startIcon'),
            pWidth: '100%',
            pLayoutMode: 'horizontal',
            pItem: cardResource,
            pOnClick: () => {
              promptAction.showToast({ message: 'Hello ImageCard' })
            },
            customBuilder: this.ImageCardBody,
            pThis: this
          })
        })
      }
      .width('100%')
    }
    .height('100%')
  }
}
```

### 为什么选择 `Pie Design` ?

- 便利的全局色彩管理API

PieDesign的组件使用PieColorManager，利用AppStorage和PersistentStorage来存储和控制全局颜色，使颜色统一，
并且能够方便地自定义全局色彩，进行统一颜色管理。

- 统一的组件设计风格

PieDesignPro组件设计遵照HarmonyOS设计规范，是原生组件在可自定义性方面的延伸。

### 颜色系统

#### PieColor

`PieColor`用于管理和操作PieDesign使用的颜色对象，可以通过以下方法构建:

```typescript
let color1 = new PieColor(); // => rgba(0,0,0,1)
let color2 = new PieColor(1); // => rgba(1,0,0,1)        
let color3 = new PieColor(1, 1); // => rgba(1,1,0,1)
let color4 = new PieColor(1, 1, 1); // => rgba(1,1,1,1)
let color5 = PieColor.fromHex("#fff"); // => rgba(255,255,255,1)
let color6 = PieColor.fromResource(this.resourceManager, $r('app.color.some_color')); // => rgba(x,x,x,x)
```

要在开发的组件中使用PieColor时，通过预置的PIE_COLOR_CONSTANTS中的COLOR_KEY获取存于AppStorage中的16进制颜色

```
@StorageProp(PIE_COLOR_CONSTANTS.COLOR_STORAGE_KEYS.PRIMARY) primaryColor: string = new PieColor(22, 119, 255).toHex();
```

要在开发的组件中使用不同深浅的PieColor时，在获取了AppStorage中的16进制颜色后，这样使用它:

```typescript
YourComponent.color(PieColor.fromHex(primaryColor).toHex(0.5))
```

#### PieColorManager

`PieColorManager` 用于管理PieDesign全局颜色

要使用PieDesign，需要在EntryAbility中调用

```typescript
PieColorManager.getInstance()
```

此操作会向AppStorage中初始化全局颜色

要自定义颜色，可以这样操作：

```typescript
PieColorManager.getInstance().setPrimary(PieColor.fromHex('#fff'));
```

可用于自定义的方法：

- `setPrimary`
- `setSuccess`
- `setWarning`
- `setDanger`
- `setText`
- `setTextSecondary`
- `setWeak`
- `setLight`
- `setBorder`
- `setBox`
- `setBackground`

这些方法可以链式调用，当希望设置主题时，可以这样操作：

```typescript
PieColorManager.getInstance().setPrimary('#FFF').setSuccess('#FFF').save();
```

> 要查看PieColor以及全局颜色设置方法，请参考组件 `PieColorExample`

### 兼容性

API10+ (OpenHarmony 4.0+/HarmonyOS NEXT)

### 参与贡献

参与贡献请参考[贡献文档](https://gitee.com/pie-design-group/pie-design/blob/beta/Contribute.md)